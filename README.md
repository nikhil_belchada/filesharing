# Very Simple FileSharing System #

Website allow normal user to register using username, email and password, once registered user will will receive and email with activation link after which user will be active and can login and start uploading file.

System also have admin user who can see all user files.

### System Setup ###
* Pull the repo
* Setup virtualenv
* Create and activate virtualenv
* install requirements file
`pip install requirements.txt`
* Run initial migrations
`python manage.py migrate`
* Initially create a super user (which will act as admin)
`python manage.py createsuperuser`
* To allow email feature following environment variable need to be set (using gmail smtp)
`EMAIL_HOST_USER` `EMAIL_HOST_PASSWORD`
* Start the server (default port 8000)
`python manage.py runserver`
* Navigate to `localhost:8000` on browser to start using website