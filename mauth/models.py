from __future__ import unicode_literals

import uuid

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager,
    AbstractUser,
)


class MyUserManager(BaseUserManager):
    def create_user(self, username, email, password):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username=username,
            email=email,
            password=password,
        )
        user.is_superuser = True
        user.is_staff = True
        user.activated = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    activated = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4)

    objects = MyUserManager()
