from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import (
    RequestContext,
    render_to_response,
    HttpResponseRedirect,
)
from django.views.generic import View
from django.contrib import auth
from mauth.models import User
from django.core.mail import send_mail

from upload.models import Upload


def send_activation_email(request, username, uuid):
    subject = 'FileSharing - Account Activation',
    body = "Hello,\n\nClick on link to activate account "
    url = (
        'http://' + request.get_host() +
        '/activation/?key=' + str(uuid) + '&username=' + username
    )
    send_mail(
        subject,
        body + url,
        'accounts@filesharing.com',
        ['nick.gokuz@gmail.com'],
        fail_silently=False,
    )

class IndexView(LoginRequiredMixin, View):
    def get(self, request):
        user_uploads = Upload.get_uploads(request.user)
        return render_to_response('home.html',
                                  {'uploads': user_uploads},
                                  context_instance=RequestContext(request))


class AccountActivationView(View):
    def get(self, request):
        params = request.GET
        username = params.get('username')
        uuid = params.get('key')
        valid = False

        try:
            user = User.objects.get(username=username, uuid=uuid)
            user.activated = True
            user.save()
            valid = True
        except User.DoesNotExist:
            pass

        return render_to_response('activation.html',
                                  {'success': valid},
                                  context_instance=RequestContext(request))


class LoginView(View):
    """
    Custom view to handle login request
    """
    def post(self, request):
        post_data = request.POST
        username = post_data.get('username')
        password = post_data.get('password')
        user = auth.authenticate(username=username, password=password)
        if user:
            auth.login(request, user)
            return HttpResponseRedirect('/')

        return render_to_response('login.html',
                                  context={'invalid': 'invalid credential'},
                                  context_instance=RequestContext(request))

    def get(self, request):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/')
        return render_to_response('login.html',
                                  context_instance=RequestContext(request))


class RegisterView(View):
    def post(self, request):
        post_data = request.POST
        username = post_data.get('username')
        email = post_data.get('email')
        password = post_data.get('password')
        if username and password and email:
            try:
                user = User.objects.create_user(username, None, password)
            except Exception as e:
                print e
                return render_to_response(
                    'register.html',
                    context={'register': False,
                             'message': 'User already exist'},
                    context_instance=RequestContext(request))

            # TODO: Add send email logic
            send_activation_email(request, user.username, user.uuid)

            return render_to_response('register.html',
                                      context={'register': True},
                                      context_instance=RequestContext(request))
        else:
            return render_to_response(
                'register.html',
                context={'register': False,
                         'message': 'All fields are compulsory'},
                context_instance=RequestContext(request))

    def get(self, request):
        return render_to_response('register.html',
                                  context_instance=RequestContext(request))


class LogoutView(View):
    """
    Custom view to handle logout request
    """
    def get(self, request):
        user = request.user
        if user.is_authenticated():
            auth.logout(request)
        return HttpResponseRedirect('/')
