from __future__ import unicode_literals

import os

from django.db import models
from mauth.models import User


class Upload(models.Model):
    user = models.ForeignKey(User, related_name='user_upload')
    document = models.FileField(upload_to='documents/')
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return ("{0}, {1}").format(self.user.username, self.filename)

    @classmethod
    def get_uploads(cls, user):
        base_query = models.Q()
        if not user.is_superuser:
            base_query &= models.Q(user=user)

        return cls.objects.filter(base_query)

    def filename(self):
        return os.path.basename(self.document.name)
