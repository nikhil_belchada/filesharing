from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name',
                                      'last_name')}),
        ('Permissions', {'fields': ('is_active',
                                    'activated',
                                    'is_staff',
                                    'is_superuser',
                                    'groups',
                                    'user_permissions')}),
        ('Important dates', {'fields': ('last_login',
                                        'date_joined')}),
    )
    list_display = ('username', 'activated', 'is_staff')


admin.site.register(User, CustomUserAdmin)
