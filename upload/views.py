from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import (
    RequestContext,
    render_to_response,
    HttpResponseRedirect,
    render,
)
from django.views.generic import View
from django.contrib import auth
from django.contrib.auth.models import User

from .forms import UploadForm

class FileUploadView(LoginRequiredMixin, View):
    def get(self, request):
        form = UploadForm()
        return render(request, 'upload.html', {'form': form})

    def post(self, request):
        print request.POST
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            upload = form.save(commit=False)
            upload.user = request.user
            upload.save()
            return HttpResponseRedirect('/')

        return render(request, 'upload.html', {'form': form})
